


clearinfo

# necessaire de garder la ligne suivante ? mettre chemin vers les resultats
#D:\temp\anr_voxcrim\ptsvox\44100_mono

dossier$ = "output"
sousdossier$ = "fichiers"
file$ = "resultats_soleil_brillance.txt"


# pourquoi les single ' ? A l'intérieur des "  "

outputfile$ = "'dossier$'" + "/" + file$
if fileReadable (outputfile$)
	fileappend "'outputfile$'" 'newline$' 
else
# add phoneme and their respective cpps, spectral_slope & f0 means 
fileappend "'outputfile$'" mot'tab$'locuteur'tab$'sexe'tab$'session'tab$'phoneme'tab$'cpps_mean'tab$'spectral_slope_mean'tab$'f0'tab$'start_selection'tab$'end_selection'newline$'
endif


# Pas possible d'automatiser ?
 ## après sélection  avec View et Edit ##

start_selection = Get start of selection
start_selection = round(start_selection*1000)/1000
end_selection = Get end of selection
end_selection = round(end_selection*1000)/1000
mid_selection = (start_selection + end_selection)/2


# on récupère les infos sur le fichier (locuteur,sexe, session)
#E ex. PTSVOX_LG001_F_session1_mic_S_1

sound = selected("Sound")
printline 'sound selected '
grille = selected("TextGrid")
printline 'TextGrid selected'

# selectionne le nom du fichier TextGrid 
grille$ = selected$("TextGrid")
printline 'grille$'

### make procedure to get info from fileTitle'

index_tiret_bas = index(grille$, "_")
reste$ = right$(grille$, length(grille$) - index_tiret_bas)
index_tiret_bas = index(reste$, "_")
locuteur$ = left$(reste$, index_tiret_bas-1)
reste$ = right$(reste$, length(reste$) - index_tiret_bas)
index_tiret_bas = index(reste$, "_")
sexe$ = left$(reste$, index_tiret_bas-1)
reste$ = right$(reste$, length(reste$) - index_tiret_bas)
index_tiret_bas = index(reste$, "_")
session$ = left$(reste$, index_tiret_bas-1)

#pause 'sexe$'//'session$'
printline reste = 'reste$' ,  locuteur = 'locuteur$'  , sexe = 'sexe$'   ,    session =  'session$'


select sound
extrait_son = Extract selected sound (preserve times)
select grille 
extrait_grille = Extract selected TextGrid (preserve times)

select grille 
extrait_grille_sans_temps = Extract selected TextGrid (time from 0)

endeditor


###############

select 'extrait_grille'
interval_mot = Get interval at time: 3, mid_selection
printline  interval_mot = 'interval_mot'
mot$ = Get label of interval: 3, interval_mot
printline mot = 'mot$'
nb_intervals = Get number of intervals: 1
printline nb_intervals  = 'nb_intervals'

## pour chaque phoneme

for a from 1 to nb_intervals
	select 'extrait_grille'
	phoneme$ = Get label of interval: 1, a
	printline label = 'phoneme$'
	start_phoneme = Get start time of interval: 1, a
	end_phoneme = Get end time of interval: 1, a


	if index_regex("'phoneme$'","[oo|ll|ai|yy|ww|aa|zz|au]")>0
		printline found phoneme

		# on récup +- 15 ms pour chaque phonème # avec découpage son Kaiser 

		mid_phoneme = (start_phoneme + end_phoneme) /2
		printline mid_phoneme = 'mid_phoneme'
		debut_analyse = mid_phoneme - 0.020
		fin_analyse = mid_phoneme + 0.020

		printline begin_analysis = 'debut_analyse'   ;	end_analysis = 'fin_analyse'

		select 'extrait_son'	
		extraction = Extract part: debut_analyse , fin_analyse, "Kaiser2", 2, "no"
		pitch = To Pitch: 0, 75, 600
		select 'pitch'

#pause 'phoneme$' 'start_phoneme'  'end_phoneme'
		#f0_mean = Get mean: debut_analyse, fin_analyse, "Hertz"
		f0_mean = Get mean: 0, 0, "Hertz"

		printline f0_mean = 'f0_mean:0'
	


		# CPPS calculates voice hoarseness 

		select 'extraction'
		powercepstrogram = To PowerCepstrogram: 60, 0.002, 5000, 50
		#Get CPPS: "yes", 0.02, 0.0005, 60, 330, 0.05, "Parabolic", 0.001, 0.05, "Exponential decay", "Robust slow"

		cpps_mean = Get CPPS: "no", 0.01, 0.001, 60, 330, 0.05, "Parabolic", 0.001, -0, "Straight", "Robust"

		printline cpps_mean = 'cpps_mean'

		#  Long-Term Average Spectrum

		if f0_mean <> undefined

		select 'extraction'
		ltas =  To Ltas (pitch-corrected): 75, 600, 5000, 100, 0.0001, 0.02, 1.3
		spectral_slope_mean = Get slope: 0, 2000, 2000, 4000, "energy"
		
		printline spectral_slope_mean = 'spectral_slope_mean'

		endif

		fileappend "'outputfile$'" 'mot$''tab$''locuteur$''tab$''sexe$''tab$''session$''tab$''phoneme$''tab$''cpps_mean:3''tab$''spectral_slope_mean:3''tab$''f0_mean:3''tab$''debut_analyse:3''tab$''fin_analyse:3''newline$'
		# start_selection and end_selection = début et fin de phonème ?
		
		nocheck select 'ltas'
		nocheck plus 'powercepstrogram'
		#plus 'extrait_son'
		# on supprime plutôt 'extraction' ?
		plus 'extraction'
		#plus 'extrait_grille'
		plus 'pitch'
		Remove

	endif

# pause phoneme analyse 'phoneme$'


endfor


###############


save_wav$ = "'dossier$'" + "/" + "'sousdossier$'" + "/" +  grille$ + "_" + "'mot$'" + "_" + "'start_selection'" + ".wav" 
select 'extrait_son'
Save as WAV file: save_wav$
save_grille$ = "'dossier$'" + "/" + "'sousdossier$'" + "/" + grille$ + "_" + "'mot$'" + "_" +  "'start_selection'" + ".TextGrid" 
#select 'extrait_grille'
select 'extrait_grille_sans_temps'
Save as text file: save_grille$

select 'extrait_son'
plus 'extrait_grille'
plus 'extrait_grille_sans_temps'
Remove

#fileappend "'outputfile$'" 'mot$''tab$''locuteur$''tab$''sexe$''tab$''session$''tab$''phoneme$''tab$''cpps_mean:3''tab$''spectral_slope_mean:3''tab$''f0_mean:3''tab$''debut_analyse:3''tab$''fin_analyse:3''newline$'

select 'sound'
plus 'grille'



####################   Paste history (for syntax ) #####




############# Extra links 

# https://www.fon.hum.uva.nl/praat/manual/Scripting_7_2__Scripting_an_editor_from_within.html
	






