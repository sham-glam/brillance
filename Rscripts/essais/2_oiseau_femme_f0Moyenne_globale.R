# f0 F oiseau
# lecture des données oiseau
library(readxl)
library(tidyverse)
library(ggplot2)
library(ggrepel)


dataOiseau <- read.table("../shamglam/Documents/stagePhonetique/praatScript/brillance/oiseau/output/resultats_oiseau_brillance.txt", sep="\t", 
               blank.lines.skip = TRUE ,  row.names = NULL, fill= TRUE, header = TRUE , stringsAsFactors=FALSE)


# lecture des valeurs avec corrections du type numeric , femme
# FILTER femme , locuteur 
filtered <- dataOiseau %>% 
  filter(sexe =='F') %>% 
  mutate(f0 = as.numeric(f0), 
         spectral_slope = as.numeric(spectral_slope_mean)) %>% 
  select(locuteur, phoneme, cpps_mean, f0, spectral_slope) %>% 
  drop_na(phoneme, cpps_mean, f0, spectral_slope) %>% 
  filter(phoneme %in% c('ww', 'aa', 'zz', 'au'))
  

summary(filtered)
view(filtered)

### on peut alléger l'étape au-dessus pour lire que des valeurs f0
f0_values <- filtered %>% 
  select(phoneme, f0, locuteur)

########################################
#class(f0_values$phoneme)
f0_values <- f0_values %>% 
  mutate(phoneme = as_factor(phoneme))


#######################################
# regroupement par locuteur / par phoneme
filtered <- filtered %>% 
  mutate(phoneme = as_factor(phoneme)) %>% 
  group_by(locuteur, phoneme) %>%
  summarise(Lower = min(f0),
            f0_moyenne = mean(f0),
            Upper = max(f0))

#View(filtered)


# visualisation 1 pour choisir des valeurs similaires de f0

filtered %>% 
  ggplot(aes(phoneme, f0_moyenne, fill = locuteur))+
  geom_bar( position = position_dodge(), 
            colour = 'white', stat = 'identity') +
  
  geom_text(aes(phoneme, label=locuteur), position = position_dodge(0.9),
            color='black', angle=90, size=2, hjust=2,
            check_overlap = TRUE)+
  stat_summary(aes(xintercept=stat(x), y=0), fun = mean, geom = 'vline')+
  labs(title= 'oiseau : valeurs moyennes f0 - Femme', 
       y= 'f0 moyenne')


# visualisation 2  - courbes

# creation d'une df pour geom_text 
label_points <- filtered %>% 
                    select(locuteur, phoneme, f0_moyenne) %>% 
                    group_by(locuteur) %>%  
                    filter(phoneme =='au') %>% 
                    summarise(x=phoneme,
                      y_max = max(f0_moyenne))
#view(label_points)

### graphique courbe 

filtered %>% 
  ggplot(aes(phoneme, f0_moyenne, color=locuteur, group=locuteur, label=locuteur)) +
  geom_point(alpha=0.8) + geom_smooth(size=0.5)+
  theme_bw() +
  geom_text( aes(x, y_max, label=locuteur), data=label_points,
             check_overlap = TRUE,
             size=1.5,  hjust=-0.3)+
  #facet_wrap(~locuteur) +   # cote à cote
  labs(title= 'oiseau : valeurs moyennes f0 - Femme', 
       y= 'f0 moyenne')

